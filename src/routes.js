import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AddEditEmployee from './pages/AddEditEmployee/AddEditEmployee';
import AddEditProduct from './pages/AddEditProduct/AddEditProduct';
import EditEmployee from './pages/EditEmployee/EditEmployee';
import EditProduct from './pages/EditProduct/EditProduct';
import Employee from './pages/Employee/Employee';
import EmployeeList from './pages/EmployeeList/EmployeeList';
import EmployeePreview from './pages/EmployeePreview/EmployeePreview';
import Home from './pages/Home/Home';
import Login from './pages/Login/Login';
import ProductList from './pages/ProductList/ProductList';
import ProductPreview from './pages/ProductPreview/ProductPreview';
import Register from './pages/Register/Register';

const RouteApp = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route path='/' element={<Home />} />
        <Route path='/employee' element={<Employee />} />
        <Route path='/product' element={<AddEditProduct />} />
        <Route path='/product/:idProduct' element={<EditProduct />} />
        <Route path='/add-employee' element={<AddEditEmployee />} />
        <Route path='/employee/:idEmployee' element={<EditEmployee />} />
        <Route path='/product-list' element={<ProductList />} />
        <Route path='/product-preview/:idProduct' element={<ProductPreview />} />
        <Route path='/employee-list' element={<EmployeeList />} />
        <Route path='/employee-preview/:idEmployee' element={<EmployeePreview />} />
      </Routes>
    </BrowserRouter>
  )
};

export default RouteApp;