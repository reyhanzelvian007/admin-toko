import style from "./NavbarTitle.module.css";
import { Link } from "react-router-dom";

const NavbarTitle = (props) => {
	return (
		<>
			<nav>
				<div className={style.leftNavbar}>
					<Link to="/">
						<div className={style.logo}>
							<h1 className={style.logoTitle}>Costume Rent</h1>
						</div>
					</Link>
				</div>
				<p className={style.navTitle}>{props.title}</p>
			</nav>
		</>
	);
};

NavbarTitle.defaultProps = {
	title: "add title here",
};

export default NavbarTitle;
