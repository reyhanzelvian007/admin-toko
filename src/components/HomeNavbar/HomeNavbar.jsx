import style from "./HomeNavbar.module.css";
import { Link } from "react-router-dom";

const HomeNavbar = () => {
	return (
		<>
			<nav>
				<div className={style.leftNavbar}>
					<Link to="/">
						<div className={style.logo}>
							<h1 className={style.logoTitle}>Costume Rent</h1>
						</div>
					</Link>
				</div>
				<Link to="/login" className={style.buttonLoginContainer}>
					<button className={style.btnMasuk}>Masuk</button>
				</Link>
			</nav>
		</>
	);
};

export default HomeNavbar;
