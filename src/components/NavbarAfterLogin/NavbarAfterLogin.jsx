import style from "./NavbarAfterLogin.module.css";
import { Link } from "react-router-dom";

const NavbarAfterLogin = () => {
	const handleLogout = () => {
		localStorage.removeItem("token");
	};

	return (
		<>
			<nav>
				<div className={style.leftNavbar}>
					<Link to="/">
						<div className={style.logo}>
							<h1 className={style.logoTitle}>Costume Rent</h1>
						</div>
					</Link>
				</div>
				<button
          className={style.btnMasuk}
          onClick={handleLogout}
        >
          Logout
        </button>
			</nav>
		</>
	);
};

export default NavbarAfterLogin;
