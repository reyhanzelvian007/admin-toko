import style from "./EmployeePreview.module.css";
import NavbarTitle from "../../components/NavbarTitle/NavbarTitle";
import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Footer from "../../components/Footer/Footer";

const EmployeePreview = () => {
	const [dataProduct, setDataProduct] = useState();
	const { idEmployee } = useParams();

	const token = localStorage.getItem("token");
	const config = {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	};

	useEffect(() => {
		axios
			.get(
				`https://admin-toko-10120718.herokuapp.com/api/employee/${idEmployee}`,
				config
			)
			.then((data) => setDataProduct(data.data.data))
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<>
			<div className={style.container}>
				<NavbarTitle title="Product Preview" />
				<div className={style.card}>
					<div className={style.imgContainer}>
						<img src={dataProduct?.img_url} alt="product image" />
					</div>
					<div className={style.cardDesc}>
						<div className={style.descPoint}>
							<h5>Nama Pegawai</h5>
							<h5 className={style.item}>{dataProduct?.name}</h5>
						</div>
						<div className={style.descPoint}>
              <h5>Jenis Kelamin</h5>
							<h5 className={style.item}>{dataProduct?.gender}</h5>
						</div>
						<div className={style.descPoint}>
              <h5>Email</h5>
							<h5 className={style.item}>{dataProduct?.email}</h5>
						</div>
						<div className={style.descPoint}>
              <h5>Alamat</h5>
							<h5 className={style.item}>{dataProduct?.address}</h5>
						</div>
					</div>
				</div>
				<Footer />
			</div>
		</>
	);
};

export default EmployeePreview;
