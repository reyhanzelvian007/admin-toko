import { useEffect, useState } from "react";
import style from "./EmployeeList.module.css";
import { useNavigate, Link } from "react-router-dom";
import HomeNavbar from "../../components/HomeNavbar/HomeNavbar";
import axios from "axios";
import NavbarAfterLogin from "../../components/NavbarAfterLogin/NavbarAfterLogin";
import Footer from "../../components/Footer/Footer";

const EmployeeList = () => {
	const navigate = useNavigate();
	const [allData, setAllData] = useState();

	const token = localStorage.getItem("token");
	const config = {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	};

	console.log(allData);

	useEffect(() => {
		axios
			.get("https://admin-toko-10120718.herokuapp.com/api/employee", config)
			.then((data) => setAllData(data.data.data))
			.catch((err) => {
				console.log(err);
			});
		if (token == null) {
			navigate('/login');
		};
	}, []);

	return (
		<>
			<div className={style.homeContainer}>
				{token ? <NavbarAfterLogin /> : <HomeNavbar />}
				<div className={style.buttonContainer}>
					<button
						className={style.buttonTambahBarang}
						onClick={() => navigate("/product")}
					>
						Tambah Barang
					</button>
					<button
						className={style.buttonTambahKaryawan}
						onClick={() => navigate("/employee")}
					>
						Tambah Karyawan
					</button>
				</div>
				<div className={style.cardContainer}>
					{allData?.map((item) => (
						<div className={style.card} key={item?.id}>
							<Link to={`/employee-preview/${item?.id}`}>
								<img src={item?.img_url} alt="product-image" />
							</Link>
							<div className={style.cardDesc}>
								<h5>{item?.name}</h5>
								<p>{item?.gender}</p>
								<h5>{item?.email}</h5>
							</div>
						</div>
					))}
				</div>
				<Footer />
			</div>
		</>
	);
};

export default EmployeeList;
