import NavbarTitle from "../../components/NavbarTitle/NavbarTitle";
import style from "./EditProduct.module.css";
import axios from "axios";
import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Footer from "../../components/Footer/Footer";

const EditProduct = () => {
	const [dataProduct, setDataProduct] = useState({});
	const { idProduct } = useParams();
	const navigate = useNavigate();

	const handleSubmit = async (e) => {
		e.preventDefault();
		let formData = new FormData();
		formData.append("name", dataProduct.name);
		formData.append("price", dataProduct.price);
		formData.append("category", dataProduct.category);
		formData.append("description", dataProduct.description);
		formData.append("image", dataProduct.image);
		const { data } = await axios({
			method: "put",
			url: `https://admin-toko-10120718.herokuapp.com/api/product/${idProduct}`,
			data: formData,
			headers: {
				Authorization: `Bearer ${token}`,
				"Content-Type": "multipart/form-data",
			},
		});
	};

	const handleDelete = async (e) => {
		e.preventDefault();
		const data = await axios({
			method: "delete",
			url: `https://admin-toko-10120718.herokuapp.com/api/product/${idProduct}`,
			headers: {
				Authorization: `Bearer ${token}`
			}
		});
		navigate('/');
	};

	const handleCategory = (e) => {
		setDataProduct({ ...dataProduct, category: e.target.value });
	};

	const handleDescription = (e) => {
		setDataProduct({ ...dataProduct, description: e.target.value });
	};

	const handleImage = (e) => {
		setDataProduct({ ...dataProduct, image: e.target.files[0] });
	};

	const token = localStorage.getItem("token");
	const config = {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	};

	useEffect(() => {
		axios
			.get(
				`https://admin-toko-10120718.herokuapp.com/api/product/${idProduct}`,
				config
			)
			.then((data) => setDataProduct(data.data.data))
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<>
			<div className={style.container}>
				<NavbarTitle title="Product Info" />
				<img
						src={dataProduct.img_url}
						alt="product-image"
						className={style.imgProduct}
					/>
				<div className={style.content}>
					<form>
						<div className={style.inputForm}>
							<label htmlFor="productName">Nama Produk</label>
							<input
								type="text"
								className={style.inputBox}
								name="produk"
								placeholder="Nama Produk"
								value={dataProduct.name}
								onChange={(e) =>
									setDataProduct({ ...dataProduct, name: e.target.value })
								}
							/>
						</div>
						<div className={style.inputForm}>
							<label htmlFor="productPrice">Harga Produk</label>
							<input
								type="text"
								className={style.inputBox}
								name="produk"
								placeholder="Rp 0,00"
								value={dataProduct.price}
								onChange={(e) =>
									setDataProduct({ ...dataProduct, price: e.target.value })
								}
							/>
						</div>
						<div className={style.inputForm}>
							<label htmlFor="productCategory">Kategori Produk</label>
							<div>
								<select
									className={style.inputBox}
									onChange={(e) => handleCategory(e)}
								>
									<option value="" disabled selected>
										Pilih Kategori
									</option>
									<option value="anime">Anime</option>
									<option value="movies">Movies</option>
									<option value="character">Character</option>
								</select>
							</div>
						</div>
						<div className={style.inputForm}>
							<label htmlFor="productDescription">Deskripsi Produk</label>
							<div>
								<select
									className={style.inputBox}
									onChange={(e) => handleDescription(e)}
								>
									<option value="" disabled selected>
										Pilih Deskripsi
									</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
							</div>
						</div>
						<div className={style.inputForm}>
							<label className={style.inputPhoto} htmlFor="inputImage">
								Masukkan Gambar
							</label>
							<input
								className={style.inputImage}
								type="file"
								alt="Box Tambah Gambar"
								onChange={(e) => handleImage(e)}
							/>
						</div>
						<div className={style.buttonContainer}>
							<button
								className={style.btnForm}
								onClick={(e) => handleSubmit(e)}
							>
								Terbitkan
							</button>
							<button
								className={style.btnHapus}
								onClick={(e) => handleDelete(e)}
							>
								Hapus
							</button>
						</div>
					</form>
				</div>
				<Footer />
			</div>
		</>
	);
};

export default EditProduct;
