import NavbarTitle from "../../components/NavbarTitle/NavbarTitle";
import style from "./AddEditEmployee.module.css";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Footer from "../../components/Footer/Footer";

const AddEditEmployee = () => {
	const [token, setToken] = useState();
	const [dataEmployee, setDataEmployee] = useState({});
	const navigate = useNavigate();

	const handleSubmit = async (e) => {
		e.preventDefault();
		let formData = new FormData();
		formData.append("name", dataEmployee.name);
		formData.append("gender", dataEmployee.gender);
		formData.append("email", dataEmployee.email);
		formData.append("address", dataEmployee.address);
		formData.append("image", dataEmployee.image)
		const { data } = await axios({
			method: "post",
			url: "https://admin-toko-10120718.herokuapp.com/api/employee",
			data: formData,
			headers: {
				Authorization: `Bearer ${token}`,
				"Content-Type": "multipart/form-data"
			},
		});
		navigate('/employee');
	};

	const handleGender = (e) => {
		setDataEmployee({ ...dataEmployee, gender: e.target.value });
	};

	const handleImage = (e) => {
		setDataEmployee({ ...dataEmployee, image: e.target.files[0] });
	};

	useEffect(() => {
		setToken(localStorage.getItem("token"));
	}, []);

	return (
		<>
			<div className={style.container}>
				<NavbarTitle title="Employee Info" />
				<div className={style.content}>
					<form>
						<div className={style.inputForm}>
							<label htmlFor="employeeName">Nama Pegawai</label>
							<input
								type="text"
								className={style.inputBox}
								name="pegawai"
								placeholder="Jajang Suruhul"
								value={dataEmployee.name}
								onChange={(e) =>
									setDataEmployee({ ...dataEmployee, name: e.target.value })
								}
							/>
						</div>
						<div className={style.inputForm}>
							<label htmlFor="employeeEmail">Email</label>
							<input
								type="email"
								className={style.inputBox}
								name="email"
								placeholder="ruhul@here.com"
								value={dataEmployee.email}
								onChange={(e) =>
									setDataEmployee({ ...dataEmployee, email: e.target.value })
								}
							/>
						</div>
						<div className={style.inputForm}>
							<label htmlFor="employeeAddress">Alamat</label>
							<input
								type="text"
								className={style.inputBox}
								name="address"
								placeholder="Jalan Cikudapateuh No.78"
								value={dataEmployee.address}
								onChange={(e) =>
									setDataEmployee({ ...dataEmployee, address: e.target.value })
								}
							/>
						</div>
						<div className={style.inputForm}>
							<label htmlFor="genderEmployee">Jenis Kelamin</label>
							<div>
								<select
									className={style.inputBox}
									onChange={(e) => handleGender(e)}
								>
									<option value="" disabled selected>
										Jenis Kelamin
									</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
							</div>
						</div>
						<div className={style.inputForm}>
							<label className={style.inputPhoto} htmlFor="inputImage">
								Masukkan Gambar
							</label>
							<input
									className={style.inputImage}
									type="file"
									alt="Box Tambah Gambar"
									onChange={(e) => handleImage(e)}
							/>
						</div>
						<div className={style.buttonContainer}>
							<button
								className={style.btnForm}
								onClick={(e) => handleSubmit(e)}
							>
								Terbitkan
							</button>
						</div>
					</form>
				</div>
				<Footer />
			</div>
		</>
	);
};

export default AddEditEmployee;
