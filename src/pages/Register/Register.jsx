import style from "./Register.module.css";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import Footer from "../../components/Footer/Footer";

const Register = () => {
	const [email, setEmail] = useState();
	const [password, setPassword] = useState();
	const [username, setUsername] = useState();
	const navigate = useNavigate();

	const handleRegisterEmail = async (e) => {
		e.preventDefault();
		const { data } = await axios({
			method: "post",
			url: `https://admin-toko-10120718.herokuapp.com/api/register`,
			data: {
				username: username,
				email: email,
				password: password
			}
		});
		navigate('/login')
	};

	return (
		<>
			<div className={style.container}>
				<div className={style.registerCard}>
					<h2>Welcome !</h2>
					<h1>Register</h1>
					<form method="post" className={style.formRegister}>
						<label htmlFor="username">Username</label>
						<input
							type="username"
							className={style.inputForm}
							name="username"
							placeholder="Username"
							value={username}
							onChange={(e) => setUsername(e.target.value)}
						/>
						<label htmlFor="email">Email</label>
						<input
							type="email"
							className={style.inputForm}
							name="email"
							placeholder="Insert your email"
							value={email}
							onChange={(e) => setEmail(e.target.value)}
						/>
						<label htmlFor="password">Password</label>
						<input
							type="password"
							className={style.inputForm}
							name="password"
							placeholder="Insert password"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
						/>
						<button
							className={style.buttonRegister}
							onClick={(e) => handleRegisterEmail(e)}
						>
							Daftar
						</button>
						<p>
							Sudah punya akun?{" "}
							<Link className={style.link} to={"/login"}>
								Login di sini
							</Link>
						</p>
					</form>
				</div>
				<Footer />
			</div>
		</>
	);
};

export default Register;
