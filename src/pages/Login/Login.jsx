import style from "./Login.module.css";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import Footer from "../../components/Footer/Footer";

const Login = () => {
	const [email, setEmail] = useState();
	const [password, setPassword] = useState();
	const navigate = useNavigate();

	const handleLoginEmail = async (e) => {
		e.preventDefault();
		const { data } = await axios({
			method: "post",
			url: `https://admin-toko-10120718.herokuapp.com/api/login`,
			data: {
				email: email,
				password: password
			}
		});
		localStorage.setItem('token', data.data);
		navigate('/');
	};

	return (
		<>
			<div className={style.container}>
				<div className={style.signInCard}>
					<h2>Welcome !</h2>
					<h1>Sign in</h1>
					<form method="post" className={style.formLogin}>
						<label htmlFor="email">Email</label>
						<input
							type="email"
							className={style.inputForm}
							name="email"
							placeholder="Insert your email"
							value={email}
							onChange={(e) => setEmail(e.target.value)}
						/>
						<label htmlFor="password">Password</label>
						<input
							type="password"
							className={style.inputForm}
							name="password"
							placeholder="Insert password"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
						/>
						<button
							className={style.buttonLogin}
							onClick={(e) => handleLoginEmail(e)}
						>
							Masuk
						</button>
						<p>
							Belum punya akun?{" "}
							<Link className={style.link} to={"/register"}>
								Daftar di sini
							</Link>
						</p>
					</form>
				</div>
				<Footer />
			</div>
		</>
	);
};

export default Login;
