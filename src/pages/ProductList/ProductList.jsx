import { useEffect, useState } from "react";
import style from "./ProductList.module.css";
import { useNavigate, Link } from "react-router-dom";
import HomeNavbar from "../../components/HomeNavbar/HomeNavbar";
import axios from "axios";
import NavbarAfterLogin from "../../components/NavbarAfterLogin/NavbarAfterLogin";
import Footer from "../../components/Footer/Footer";

const ProductList = () => {
	const navigate = useNavigate();
	const [allData, setAllData] = useState();

	const rupiah = (number) => {
		return new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		}).format(number);
	};

	const token = localStorage.getItem("token");
	const config = {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	};

	console.log(allData);

	useEffect(() => {
		axios
			.get("https://admin-toko-10120718.herokuapp.com/api/product", config)
			.then((data) => setAllData(data.data.data))
			.catch((err) => {
				console.log(err);
			});
		if (token == null) {
			navigate('/login');
		};
	}, []);

	return (
		<>
			<div className={style.homeContainer}>
				{token ? <NavbarAfterLogin /> : <HomeNavbar />}
				<div className={style.buttonContainer}>
					<button
						className={style.buttonTambahBarang}
						onClick={() => navigate("/product")}
					>
						Tambah Barang
					</button>
					<button
						className={style.buttonTambahKaryawan}
						onClick={() => navigate("/employee")}
					>
						Tambah Karyawan
					</button>
				</div>
				<div className={style.cardContainer}>
					{allData?.map((item) => (
						<div className={style.card} key={item?.id}>
							<Link to={`/product-preview/${item?.id}`}>
								<img src={item?.img_url} alt="product-image" />
							</Link>
							<div className={style.cardDesc}>
								<h5>{item?.name}</h5>
								<p>{item?.category}</p>
								<h5>{rupiah(item?.price)}</h5>
							</div>
						</div>
					))}
				</div>
				<Footer />
			</div>
		</>
	);
};

export default ProductList;
