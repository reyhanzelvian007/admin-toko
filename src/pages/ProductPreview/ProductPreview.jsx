import style from "./ProductPreview.module.css";
import NavbarTitle from "../../components/NavbarTitle/NavbarTitle";
import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Footer from "../../components/Footer/Footer";

const ProductPreview = () => {
	const [dataProduct, setDataProduct] = useState();
	const { idProduct } = useParams();

	const rupiah = (number) => {
		return new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		}).format(number);
	};

	const token = localStorage.getItem("token");
	const config = {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	};

	useEffect(() => {
		axios
			.get(
				`https://admin-toko-10120718.herokuapp.com/api/product/${idProduct}`,
				config
			)
			.then((data) => setDataProduct(data.data.data))
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<>
			<div className={style.container}>
				<NavbarTitle title="Product Preview" />
				<div className={style.card}>
					<div className={style.imgContainer}>
						<img src={dataProduct?.img_url} alt="product image" />
					</div>
					<div className={style.cardDesc}>
						<div className={style.descPoint}>
							<h5>Nama Produk</h5>
							<h5 className={style.item}>{dataProduct?.name}</h5>
						</div>
						<div className={style.descPoint}>
              <h5>Harga Produk</h5>
							<h5 className={style.item}>{rupiah(dataProduct?.price)}</h5>
						</div>
						<div className={style.descPoint}>
              <h5>Kategori Produk</h5>
							<h5 className={style.item}>{dataProduct?.category}</h5>
						</div>
						<div className={style.descPoint}>
              <h5>Deskripsi Produk</h5>
							<h5 className={style.item}>{dataProduct?.description}</h5>
						</div>
					</div>
				</div>
				<Footer />
			</div>
		</>
	);
};

export default ProductPreview;
