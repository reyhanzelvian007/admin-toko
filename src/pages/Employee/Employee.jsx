import { useEffect, useState } from "react";
import style from "./Employee.module.css";
import { useNavigate, Link } from "react-router-dom";
import HomeNavbar from "../../components/HomeNavbar/HomeNavbar";
import axios from "axios";
import Footer from "../../components/Footer/Footer";
import NavbarAfterLogin from "../../components/NavbarAfterLogin/NavbarAfterLogin";

const Employee = () => {
	const navigate = useNavigate();
	const [allData, setAllData] = useState();

	const rupiah = (number) => {
		return new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		}).format(number);
	};

	const token = localStorage.getItem("token");
	const config = {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	};

	console.log(allData);

	useEffect(() => {
		axios
			.get("https://admin-toko-10120718.herokuapp.com/api/employee", config)
			.then((data) => setAllData(data.data.data))
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<>
			<div className={style.homeContainer}>
				{token ? <NavbarAfterLogin /> : <HomeNavbar />}
				<div className={style.buttonContainer}>
					<button
						className={style.buttonTambahBarang}
						onClick={() => navigate("/")}
					>
						Tambah Barang
					</button>
					<button
						className={style.buttonTambahKaryawan}
						onClick={() => navigate("/add-employee")}
					>
						Tambah Karyawan
					</button>
				</div>
				<div className={style.cardContainer}>
					{allData?.map((item) => (
						<div className={style.card} key={item?.id}>
							<Link to={`/employee/${item?.id}`}>
								<img src={item?.img_url} alt="product-image" />
							</Link>
							<div className={style.cardDesc}>
								<h5>{item?.name}</h5>
								<p>{item?.gender}</p>
								<h5>{item?.email}</h5>
							</div>
						</div>
					))}
				</div>
				<Footer />
			</div>
		</>
	);
};

export default Employee;
